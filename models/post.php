<?php
class Post {
    // definimos tres atributos
    // los declaramos como públicos para acceder directamente $post->author
    public $id;
    public $author;
    public $content;
    public $image;
    public $timestamp;
    public $titulo;

    public function __construct($id, $author, $content, $titulo, $image) {
        $this->id = $id;
        $this->author = $author;
        $this->content = $content;
        $this->titulo = $titulo;
        $this->image = $image;
    }

    public static function all() {
        $list = [];
        $db = Db::getInstance();
        $req = $db->query('SELECT * FROM posts');
        // creamos una lista de objectos post y recorremos la respuesta de la consulta
        foreach($req->fetchAll() as $post) {
            $list[] = new Post($post['id'], $post['author'], $post['content'], $post['titulo'], $post['image']);
        }
        return $list;
    }
    public static function find($id) {
        $db = Db::getInstance();
        // nos aseguramos que $id es un entero
        $id = intval($id);
        $req = $db->prepare('SELECT * FROM posts WHERE id = :id');
        // preparamos la sentencia y reemplazamos :id con el valor de $id
        $req->execute(array('id' => $id));
        $post = $req->fetch();
        return new Post($post['id'], $post['author'], $post['content'], $post['titulo'], $post['image']);
    }

    public static function creater($insertPost) {

        $db = Db::getInstance();
        
        $req = $db->prepare('INSERT INTO posts SET author =:author, content =:content, titulo =:titulo, image =:image, created =:created, modified =:modified');
        
        $dateC = date('Y-m-d H:i:s');
        $dateM = date('Y-m-d H:i:s');

        $datos = array(
            ":author"=>htmlspecialchars(strip_tags($insertPost->author)),
            ":content"=>htmlspecialchars(strip_tags($insertPost->content)),
            ":titulo"=>htmlspecialchars(strip_tags($insertPost->titulo)),
            ":image"=>htmlspecialchars(strip_tags($insertPost->image)),
            ":created"=>htmlspecialchars(strip_tags($dateC)),
            ":modified"=>htmlspecialchars(strip_tags($dateM))
        );

        if($req->execute($datos)){
            //Inserta correctamente y crea la carpeta para las imagenes
            Post::uploadPhoto($insertPost->image);
        }else{
            //Algun error al insertar post
            header('Location: '.constant('URL')."posts/error");
        }
        //$req->execute($datos);
    }

    public static function updater($updatePost) {

        $db = Db::getInstance();
        
        $req = $db->prepare('UPDATE posts SET author =:author, content =:content, titulo =:titulo, image =:image, modified =:modified where id =:id');
        
        $dateM = date('Y-m-d H:i:s');

        $datos = array(
            ":author"=>htmlspecialchars(strip_tags($updatePost->author)),
            ":content"=>htmlspecialchars(strip_tags($updatePost->content)),
            ":titulo"=>htmlspecialchars(strip_tags($updatePost->titulo)),
            ":image"=>htmlspecialchars(strip_tags($updatePost->image)),
            ":modified"=>htmlspecialchars(strip_tags($dateM)),
            ":id"=>htmlspecialchars(strip_tags($updatePost->id))
        );

        if($req->execute($datos)){
            //Inserta correctamente y crea la carpeta para las imagenes
            Post::uploadPhoto($insertPost->image);
        }else{
            //Algun error al insertar post
            header('Location: '.constant('URL')."posts/insert/error");
        }
        //$req->execute($datos);
    }

    public static function deleter($idD) {

        $db = Db::getInstance();
        
        $req = $db->prepare('DELETE from posts where id =:id');
        $datos = array(
            ":id" => htmlspecialchars(strip_tags($idD))
        );
        return $req->execute($datos);
    }


    public static function uploadPhoto($image){
 
        $result_message="";
     
        // now, if image is not empty, try to upload the image
        if($image){
     
            // sha1_file() function is used to make a unique file name
            $target_directory = "uploads/";
            $target_file = $target_directory . $image;
            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
     
            // error message is empty
            $file_upload_error_messages="";
            // make sure that file is a real image
                $check = getimagesize($_FILES["image"]["tmp_name"]);
                if($check!==false){
                    // submitted file is an image
                }else{
                    $file_upload_error_messages.="<div>Submitted file is not an image.</div>";
                }
                
                // make sure certain file types are allowed
                $allowed_file_types=array("jpg", "jpeg", "png", "gif");
                if(!in_array($file_type, $allowed_file_types)){
                    $file_upload_error_messages.="<div>Only JPG, JPEG, PNG, GIF files are allowed.</div>";
                }
                
                // make sure file does not exist
                if(file_exists($target_file)){
                    $file_upload_error_messages.="<div>Image already exists. Try to change file name.</div>";
                }
                
                // make sure submitted file is not too large, can't be larger than 1 MB
                if($_FILES['image']['size'] > (1024000)){
                    $file_upload_error_messages.="<div>Image must be less than 1 MB in size.</div>";
                }
                
                // make sure the 'uploads' folder exists
                // if not, create it
                if(!is_dir($target_directory)){
                    mkdir($target_directory, 0777, true);
                }
                // if $file_upload_error_messages is still empty
                if(empty($file_upload_error_messages)){
                    // it means there are no errors, so try to upload the file
                    if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)){
                        // it means photo was uploaded
                    }else{
                        $result_message.="<div class='alert alert-danger'>";
                            $result_message.="<div>Unable to upload photo.</div>";
                            $result_message.="<div>Update the record to upload photo.</div>";
                        $result_message.="</div>";
                    }
                }
                
                // if $file_upload_error_messages is NOT empty
                else{
                    // it means there are some errors, so show them to user
                    $result_message.="<div class='alert alert-danger'>";
                        $result_message.="{$file_upload_error_messages}";
                        $result_message.="<div>Update the record to upload photo.</div>";
                    $result_message.="</div>";
                }
    
     
        }
     
        return $result_message;
    }
    
}
?>