<?php
class Relleno {
    // los declaramos como públicos para acceder directamente $Relleno->nombre
    public $id;
    public $nombre;
    public $enume;

    public function __construct($id, $nombre, $enume) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->enume = $enume;
    }

    public static function all() {
        $list = [];
        $db = Db::getInstance();
        $req = $db->query('SELECT * FROM rellenos');
        // creamos una lista de objectos Relleno y recorremos la respuesta de la consulta
        foreach($req->fetchAll() as $relleno) {
            $list[] = new Relleno($relleno['id'], $relleno['nombre'], $relleno['enume'], $relleno['fecha']);
        }
        return $list;
    }
    public static function find($id) {
        $db = Db::getInstance();
        // nos aseguramos que $id es un entero
        $id = intval($id);
        $req = $db->prepare('SELECT * FROM rellenos WHERE id = :id');
        // preparamos la sentencia y reemplazamos :id con el valor de $id
        $req->execute(array('id' => $id));
        $relleno = $req->fetch();
        return new Relleno($relleno['id'], $relleno['nombre'], $relleno['enume']);
    }

    public static function creater($insertRelleno) {

        $db = Db::getInstance();
        
        $req = $db->prepare('INSERT INTO rellenos SET nombre =:nombre, enume =:enume, fecha =:fecha');

        $fecha = date('Y-m-d H:i:s');
       
        $datos = array(
            ":nombre"=>htmlspecialchars(strip_tags($insertRelleno->nombre)),
            ":enume"=>htmlspecialchars(strip_tags($insertRelleno->enume)),
            ":fecha"=>htmlspecialchars(strip_tags($fecha))
        );
        
        if($req->execute($datos)){
            header('Location: '.constant('URL')."rellenos/index");
        }else{
            //Algun error al insertar relleno
            header('Location: '.constant('URL')."rellenos/error");
        }
        
    }

    public static function updater($updateRelleno) {

        $db = Db::getInstance();
        
        $req = $db->prepare('UPDATE rellenos SET nombre =:nombre, enume =:enume where id =:id');
        
        //$dateM = date('Y-m-d H:i:s');

        $datos = array(
            ":nombre"=>htmlspecialchars(strip_tags($updateRelleno->nombre)),
            ":enume"=>htmlspecialchars(strip_tags($updateRelleno->enume)),
            ":id"=>htmlspecialchars(strip_tags($updateRelleno->id))
        );

        if($req->execute($datos)){
            header('Location: '.constant('URL')."rellenos/index");
        }else{
            //Algun error al insertar relleno
            header('Location: '.constant('URL')."rellenos/error");
        }
        //$req->execute($datos);
    }

    public static function deleter($idD) {

        $db = Db::getInstance();
        
        $req = $db->prepare('DELETE from rellenos where id =:id');
        $datos = array(
            ":id" => htmlspecialchars(strip_tags($idD))
        );
        return $req->execute($datos);
    }
    
}
?>