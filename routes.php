<?php
    function call($controller, $action, $parametro) {
        require_once('controllers/' . $controller . '_controller.php');
        switch($controller) {
            case 'pages':
                $controller = new PagesController();
                break;
            case 'posts':
                // necesitamos el modelo para después consultar a la BBDD desde el controlador
                require_once('models/post.php');
                $controller = new PostsController();
                break;
            case 'rellenos':
                // necesitamos el modelo para después consultar a la BBDD desde el controlador
                require_once('models/relleno.php');
                $controller = new RellenoController();
                break;
        }
            // llama al método guardado en $action y le paso el parametro (id)
            $controller->{ $action }($parametro);
    }
    // lista de controladores que tenemos y sus acciones
    // consideramos estos valores "permitidos"
    // agregando una entrada para el nuevo controlador y sus acciones.
    $controllers = array( 'pages' => ['home', 'error'],
                        'posts' => ['index', 'show', 'insertView', 'insertPost', 'updateView', 'updatePost', 'deletePost', 'error'],
                        'rellenos' => ['index', 'show', 'insertView', 'insertRelleno', 'updateView', 'updateRelleno', 'deleteRelleno', 'error']);
    // verifica que tanto el controlador como la acción solicitados estén permitidos
    // Si alguien intenta acceder a otro controlador y/o acción, será redirigido al método de error del controlador de pages.
    if (array_key_exists($controller, $controllers)) {
        if (in_array($action, $controllers[$controller])) {
            call($controller, $action, $parametro);
        } else {
            call('pages', 'error',"");
        }
    } else {
        call('pages', 'error',"");
    }
?>