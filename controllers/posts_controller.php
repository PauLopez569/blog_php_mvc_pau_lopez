<?php
class PostsController {
    public function index() {
        // Guardamos todos los posts en una variable
        $posts = Post::all();
        require_once('views/posts/index.php');
    }
    public function show($id) {
        // esperamos una url del tipo ?controller=posts&action=show&id=x
        // si no nos pasan el id redirecionamos hacia la pagina de error, el id tenemos que buscarlo en la BBDD
        if (!isset($id)) {
            return call('pages', 'error');
        }
        // utilizamos el id para obtener el post correspondiente
        $post = Post::find($id);
        require_once('views/posts/show.php');
    }

    public function insertView(){
        require_once('views/posts/create.php');
    }

    public function insertPost(){
        $authorP = $_POST['author'];
        $contentP = $_POST['content'];
        $tituloP = $_POST['titulo'];
        $imageP = !empty($_FILES["image"]["name"]) ? sha1_file($_FILES['image']['tmp_name']) . "-" . basename($_FILES["image"]["name"]) : "";

        if(!empty($_POST)){
            if(isset($_POST['author']) && isset($_POST['content']) && isset($_POST['titulo'])){
                $insert = new Post(-1, $authorP, $contentP, $tituloP, $imageP);
                Post::creater($insert);
                header ("Location: " .constant("URL")."posts/index");
            }
        }
        return call('posts', 'error');
    }

    public function updateView($id) {
        if (!empty($id)) {
            $post = Post::find($id);
            require_once 'views/posts/update.php';
        }else{
            return call('posts', 'error');
        }   
    }
    
    // Modifica el post que se le envie
    public function updatePost() {
        $idP = $_POST["id"];
        $authorP = $_POST['author'];
        $contentP = $_POST['content'];
        $tituloP = $_POST['titulo'];
        $imageP = !empty($_FILES["image"]["name"]) ? sha1_file($_FILES['image']['tmp_name']) . "-" . basename($_FILES["image"]["name"]) : "";
        //$dateM = date('Y-m-d H:i:s');
   
        if(!empty($_POST)){
            if(isset($_POST['id']) && isset($_POST['author']) && isset($_POST['content']) && isset($_POST['titulo'])){
                $update = new Post($idP, $authorP, $contentP, $tituloP, $imageP);
                Post::updater($update);
                header ("Location: " .constant("URL")."posts/index");
            }
        }
        return call('posts', 'error');
    }

    // Borra el post que se le envie
    public function deletePost($id) {
        if (empty($id)) {
            return call('posts', 'error', "Error: No existe este post");
        }
        if (Post::deleter($id)) {
            header('Location: '.constant('URL')."posts/index");
        }
        else {
            return call('posts', 'error');
        }
    }
    

}
?>