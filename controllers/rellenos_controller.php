<?php
class RellenoController {
    public function index() {
        // Guardamos todos los rellenos en una variable
        $rellenos = Relleno::all();
        require_once('views/rellenos/index.php');
    }
    public function show($id) {
        // esperamos una url del tipo ?controller=rellenos&action=show&id=x
        // si no nos pasan el id redirecionamos hacia la pagina de error, el id tenemos que buscarlo en la BBDD
        if (!isset($id)) {
            return call('pages', 'error');
        }
        // utilizamos el id para obtener el relleno correspondiente
        $relleno = Relleno::find($id);
        require_once('views/rellenos/show.php');
    }

    public function insertView(){
        require_once('views/rellenos/create.php');
    }

    public function insertRelleno(){
        $nombreP = $_POST['nombre'];
        $enumeP = $_POST['enume'];
        //$imageP = !empty($_FILES["image"]["name"]) ? sha1_file($_FILES['image']['tmp_name']) . "-" . basename($_FILES["image"]["name"]) : "";

        if(!empty($_POST)){
            if(isset($_POST['nombre']) && isset($_POST['enume'])){
                $insert = new Relleno(-1, $nombreP, $enumeP, $fechaP);
                Relleno::creater($insert);
                header ("Location: " .constant("URL")."rellenos/index");
            }
        }
        return call('rellenos', 'error');
    }

    public function updateView($id) {
        if (!empty($id)) {
            $relleno = Relleno::find($id);
            require_once 'views/rellenos/update.php';
        }else{
            return call('rellenos', 'error');
        }   
    }
    
    // Modifica el relleno que se le envie
    public function updateRelleno() {
        $idP = $_POST["id"];
        $nombreP = $_POST['nombre'];
        $enumeP = $_POST['enume'];
   
        if(!empty($_POST)){
            if(isset($_POST['nombre']) && isset($_POST['enume'])){
                $update = new Relleno($idP, $nombreP, $enumeP);
                Relleno::updater($update);
                header ("Location: " .constant("URL")."rellenos/index");
            }
        }
        return call('rellenos', 'error');
    }

    // Borra el relleno que se le envie
    public function deleteRelleno($id) {
        if (empty($id)) {
            return call('rellenos', 'error', "Error: No existe este relleno");
        }
        if (Relleno::deleter($id)) {
            header('Location: '.constant('URL')."rellenos/index");
        }
        else {
            return call('rellenos', 'error');
        }
    }
    

}
?>