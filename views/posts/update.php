<form action="<?php echo constant('URL'); ?>posts/updatePost" method="post" enctype="multipart/form-data">
    <p>
        <input type="hidden" name="id" value="<?php echo $post->id; ?>" readonly>

        <label for="author">Autor:</label><br/>
        <input type="text" name="author" value="<?php echo $post->author;?>"><br/>

        <label for="content">Post:</label><br/>
        <input type="text" name="content" value="<?php echo $post->content;?>"><br/>

        <label for="titulo">Titulo:</label><br/>
        <input type="text" name="titulo" value="<?php echo $post->titulo;?>"><br/>

        <label for="image">Foto</label>
        <input type="file" name="image" value="<?php echo $post->image;?>"/><br/>
        <img src="<?php echo constant('URL'); ?>uploads/<?php echo $post->image; ?>"><br/>
            
        <button type='submit'>Send</button>
    </p>
</form>