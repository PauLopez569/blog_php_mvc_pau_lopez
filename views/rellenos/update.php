<form action="<?php echo constant('URL'); ?>rellenos/updateRelleno" method="post" enctype="multipart/form-data">
    <p>
        <input type="hidden" name="id" value="<?php echo $relleno->id; ?>" readonly>

        <label for="nombre">Nombre:</label><br/>
        <input type="text" name="nombre" value="<?php echo $relleno->nombre;?>"><br/>

        <label for="enume">Enum:</label><br/>
        <select class="input" name="enume">
            <option value="si" <?php echo ($relleno->enume == "si") ? "selected" : "" ?>>SI</option>
            <option value="no" <?php echo ($relleno->enume == "no") ? "selected" : "" ?>>NO</option>
        </select>
      
        <button type='submit'>Send</button>
    </p>
</form>
